# Sample app for ReactJS
This is a basic react app that demonstrates how to use props, state, and use CURD operations.
The App uses both class and functional components as well as state hooks(useState()) to demonstrate their usage.
CURD operations are stored are stored in CardService.js /src folder. It is defined to make calls to an elasticsearch server on port 9200 and the development serve also runs on the same port to avoid cross origin conflicts.

#### Note:
 This app does not make use of advance concepts such as state management such as Redux and routing. This is basic level pur ReactJS made to fumction with a server script of your choice for educational purposes based on the main concepts in the ReactJS documentation at the link below. Following the main concepts will help you understand the code better.
https://reactjs.org/docs/hello-world.html
