import React from 'react';
import logo from './logo.svg';
import HeroicFeatures from "./heroicFeatures";
import QueryForm from "./QueryForm";
import './App.css';

function App() {


  let temp;
  const HandleNameChange=function(e){
    console.log(e.value);    
    temp.name=e.value;
  };
  HandleNameChange.bind(this);
  const GetName=function(){
    return temp.name;

  };

  return (
    <div className="container">
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1>Hello World!</h1>
          <p>
            Edit <code>src/App.js</code> and save to reload.
        </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
        </a>
        </header>
        <div className="row text-center">
          <HeroicFeatures title="My Card 1" text="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque." />
          <HeroicFeatures title="My Card 2" />
          <HeroicFeatures title={GetName} text="Name changes dyamically." />

        </div>
        <div className="row text-center">
          <QueryForm onNameChange={HandleNameChange}  />
        </div>

      </div>
    </div>
  );
}

export default App;
