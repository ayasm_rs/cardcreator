import React,{useState} from 'react';
import "./heroic-features.css"

const CardSubmitForm = (props) => {
    
    const [values, setValues] = useState({
        image:'http://placehold.it/500x325',
        title: 'The Title',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        btnText:"Read More",
        link: '#',
      });

      const handleChange = name => event => {
        setValues({
          ...values,
          [name]: event.target.value,
        });
      };

    return <form>
        <div className="col-12 mb-4">
            <div className="card h-100">
                <img className="card-img-top" src={values.image} alt="" />
                <input type="text" name='title' value={values.image} onChange={handleChange('image')} />
                <div className="card-body">
                    <h3 className="card-title">{values.title}</h3>
                    <input type="text" name='title' value={values.title} onChange={handleChange('title')} />
                    <p className="card-text">
                        <textarea name="body" value={values.body} onChange={handleChange('body')} />
                    </p>
                    <p>
                        <input type="text" name='btnText' value={values.btnText} onChange={handleChange('btnText')} />
                    </p>
                    <p>
                        <input type="text" name='link' value={values.link} onChange={handleChange('link')} />
                    </p>
                </div>
                <div className="card-footer">
                    <button type="reset" onClick={() => props.newCardAction(values)} className="btn btn-primary">Save!</button>
                </div>
            </div>
        </div>
    </form>
        ;
}
export default CardSubmitForm;