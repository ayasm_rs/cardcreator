import React from 'react';
import logo from './logo.svg';
import CardSubmitForm from "./CardSubmitForm";
import HeroicFeatures from "./heroicFeatures";
import QueryForm from "./QueryForm";
import './App.css';
import CardService from './CardSevice';


class App extends React.Component {
    name="";
    cs;
    cards=[];
    constructor() {
        super();
        this.HandleNameChange.bind(this);
        this.deleteCard.bind(this);
        this.HandleNewCard.bind(this);
        this.state={name:'', cards:[]};
        this.cs= new CardService()
    }

    HandleNameChange = e => {
       
        this.setState({'name':e.value});
    }

    HandleNewCard = data =>{              
        this.cs.create(data).then(()=>{
            this.cardsFromDB();
        });
        
    };

    cardsFromDB = () => {
        this.cs.read().then(
            (res)=>{
                const new_mappable=res['data']['hits']['hits'];
                const cards= new_mappable.map((cardinfo, index)=>{
                    const x = cardinfo['_source'];                                      
                    const y = <HeroicFeatures 
                    key={index} 
                    dbid={cardinfo['_id']} 
                    doDelete={this.deleteCard} 
                    image={x.image} 
                    title={x['title']} 
                    text={x['body']} 
                    link={x.link} 
                    btnText={x.btnText} />;                    

                    return y;
                });                
                this.setState({'cards':cards});

        });
    };
    deleteCard = (x,y)  => {
        const subs = this.cs.delete(y);
        subs.then(()=>this.cardsFromDB());
        
    };

   

    render() {
        return (
            <div className="container">
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo" />                       
                        <CardSubmitForm newCardAction={this.HandleNewCard}></CardSubmitForm>   
                    </header>
                    <div className="row text-center">
                    <QueryForm onNameChange={this.HandleNameChange} />
                        <button onClick={this.cardsFromDB}>Refresh</button>
                    </div>
                    <div className="row text-center">                        
                        <HeroicFeatures title={this.state.name} text="Your name in the title." btnText="more" link="http://google.com"/>
                        {this.state.cards}

                    </div>
                    

                </div>
            </div>
        );
    }
}
export default App;