import React from "react";
import "./QueryForm.css";


class QueryForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value:''
        };
    }
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Name:
                        <input type="text" name='value' value={this.state.value} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Submit" />
            </form>
        );
    }

    handleChange = (event) => {
        event.preventDefault();
        let change = {};
        change[event.target.name] = event.target.value;
        this.setState(change);
        this.props.onNameChange(change);
    }
    
    handleSubmit = (event) => {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }
}
export default QueryForm;

