import Dexie from 'dexie';
import *as axios from 'axios' 

class CardService{
    db={};
    constructor(){
        this.db = new Dexie('CardDB');
        this.db.version(1).stores({cards:'++id,image,title,body,btnText,link'});
    }
    create(payload){
        return axios({
            method: 'post',
            url: 'http://localhost:9200/carddb/cards/',
            headers:{
                "Content-Type":'application/json'
            },
        
            data: payload
          });
    }
    read(){
        return axios({
            method: 'get',
            url: 'http://localhost:9200/carddb/cards/_search',
            headers:{
                "Content-Type":'application/json'
            },       
           
          });

    }
    update(id,payload){
        return axios({
            method: 'put',
            url: 'http://localhost:9200/carddb/cards/'+id,
            headers:{
                "Content-Type":'application/json'
            },
            data: payload
        
           
          });

    }
    delete(id){
        return axios({
            method: 'delete',
            url: 'http://localhost:9200/carddb/cards/'+id,
            headers:{
                "Content-Type":'application/json'
            },
        
           
          });

    }

}
export default CardService;