import React from 'react';
import './heroic-features.css';

class HeroicFeatures extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props){
        super(props);
        this.handleBtnClick.bind(this);

    }
    render() {
      return <div className="col-lg-3 col-md-6 mb-4">
        <div className="card h-100">
          <img className="card-img-top" src={this.props.image} alt="" />
          <div className="card-body">
            <h4 className="card-title">{this.props.title}</h4>
            <p className="card-text">{this.props.text}</p>
          </div>
          <div className="card-footer">
            <button className="btn btn-warning"data-toggle="modal" data-target="#exampleModal">Edit</button>
            <a target="_blank" rel="noopener noreferrer" href={this.props.link} className="btn btn-primary">{this.props.btnText}</a>
            <button className="btn btn-danger" onClick={() => this.props.doDelete(Event, this.props.dbid)}>Delete</button>
          </div>
        </div>
        <div id="exampleModal" className="modal" tabindex="-1" role="dialog">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Modal title</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <p>Modal body text goes here.</p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>
      </div>;
    }

    handleBtnClick(e){
        e.preventDefault();
        alert('button clicked!')
    }

}

export default HeroicFeatures;